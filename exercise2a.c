#include <stdio.h>

void function_b() {
  char buffer[4];
  *(long*)(&buffer[0] + 24) = 0;  // set return address to 0
  fprintf(stdout, "Executing function_b\n");
}

void function_a() {
  int beacon = 0xa0b1c2d3;
  fprintf(stdout, "Executing function_a\n");
  function_b();
  fprintf(stdout, "Executed function_b\n");
}

int main() {
  function_a();
  fprintf(stdout, "Finished!\n");
  return 0;
}

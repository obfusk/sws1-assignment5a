.PHONY: all clean

SHELL     = bash
PROGRAMS  = exercise2a exercise2b_ exercise2c

CC        = gcc
CFLAGS    = -Wall -Wextra -std=c99 -g -fno-stack-protector \
            -fno-omit-frame-pointer

all: $(PROGRAMS)

clean:
	for p in $(PROGRAMS); do rm -f "$$p"{,.o}; done
